#ifndef __J_HPP__
#define __J_HPP__

#include "fdaPDE.h"
#include "mesh.h"
#include "regressionData.h"
#include "mixedFERegression.h"
#include "mesh_objects.h"

/**
 * @file j.h
 * @brief contains the class J
 */


/**
 * @class J
 * @brief Contains methods around the optimization of the functional J(f,K) as described in \cite Bernardi
 */
class J {
    private:
        const MeshHandler<1> & mesh_;
        const RegressionDataElliptic & regressionData_;
        MixedFERegression<RegressionDataElliptic, IntegratorTriangleP2, 1> regression_;
        const std::vector<Point> & locations_;

    public:

		/**
		 * @detail The constructor solves the optimization of the functional J using the fdaPDE package
		 */
        J(const MeshHandler<1> & mesh, const RegressionDataElliptic & regressionData, const std::vector<Point> & locations);

		/**
		 * @return The solution coefficients of the FEM basis functions for fHat.
		 */
        const std::vector<VectorXr> & getSolution() const;

		/**
		 * @return The vector of DOF for each value of regressionData_.getLambda()
		 * @detail In the paper \cite Bernardi, DOF corresponds to tr(S) (p.14)
		 */
        const std::vector<Real> & getDOF() const;

		/**
		 * @return The vector of GCV indexes for each value of regressionData_.getLambda()
		 */
        VectorXr getGCV() const;
};

#endif
