#ifndef __OPTIM_H_HPP__
#define __OPTIM_H_HPP__

#include "fdaPDE.h"
#include "h.h"
#include "solver/lbfgsbsolver.h"

/*
 * Template specialization of LbfgsbSolver
 * for H. It sets a lower
 * and upper bound in the implementation of minimize
*/

/**
 * @class
 * @brief Contains a method to minimize the functional H(K)
 */
class OptimH {
    private:
        using TVector = H::TVector;

    public:
		/**
		 * @param h The object to minimize
		 * @param start The anisotropy in vector form, where the algorithm starts
		 */
		static void minimize(H & h, TVector & start);
};


#endif
