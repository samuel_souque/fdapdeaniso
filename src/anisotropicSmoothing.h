#ifndef __ANISOTROPIC_SMOOTHING_HPP__
#define __ANISOTROPIC_SMOOTHING_HPP__

#include "fdaPDE.h"
#include "mesh.h"
#include "regressionData.h"
#include "mesh_objects.h"
#include "h.h"

/**
 * @file anisotropicSmoothing.h
 * @brief contains the class AnisotropicSmoothing implementing the main algorithm
 */

/**
 * @class AnisotropicSmoothing
 * @brief Implements the anisotropic smoothing algorithm, with estimation of the anisotropy matrix from data
 */
class AnisotropicSmoothing {
    private:
        const MeshHandler<1> & mesh_;
        const RegressionDataElliptic & regressionData_;
        const std::vector<Real> & lambda_;
        const std::vector<Point> & locations_;
		
		/**
		 * @brief creates a new RegressionData object from regressionData_, forcing the regularization coefficient to lambda
		 */
        RegressionDataElliptic createRegressionData(const Real & lambda) const;

		/**
		 * @brief creates a new RegressionData object from regressionData_, forcing the anisotropy to k and DOF_ to dof
		 */
        RegressionDataElliptic createRegressionData(const H::TVector & k, const bool dof) const;

		/**
		 * @brief creates a new RegressionData object from regressionData_, forcing the regularization coefficient to lambda and the anisotropy to k
		 */
        RegressionDataElliptic createRegressionData(const Real & lambda, const H::TVector & k) const;

    public:
		
        AnisotropicSmoothing(const MeshHandler<1> & mesh, const RegressionDataElliptic & regressionData, const std::vector<Real> & lambda, const std::vector<Point> & locations) : mesh_(mesh), regressionData_(regressionData), lambda_(lambda), locations_(locations) {}
	
		/**
		 * @brief executes the anisotropic smoothing algorithm for the problem described in the class attributes
		 * @return the first element of the pair is the vector of solution coefficients, the second element is the estimated anisotropy matrix
		 */
        std::pair<const std::vector<VectorXr>, const Eigen::Matrix<Real, 2, 2>> smooth() const;
};

#endif
