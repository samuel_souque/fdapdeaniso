#ifndef __H_HPP__
#define __H_HPP__

#include "fdaPDE.h"
#include "boundedproblem.h"
#include "mesh.h"
#include "regressionData.h"
#include "mesh_objects.h"

/**
 *	@file h.h
 *	@brief contains the class declaration of the functional H(K).
 */

/**
 * 	@class H
 *	@brief Implements the functional H(K) as described in the paper \cite Bernardi.
 *	@details We use the cppoptlib library for its LBFGSB algorithm.
 *		The class H inheritates from a cppoptlib class for compatibility reasons with cppoptlib.
 */
class H : public cppoptlib::BoundedProblem<Real, 2> {
    private:
        const MeshHandler<1> & mesh_;
        const RegressionDataElliptic & regressionData_;
        const std::vector<Point> & locations_;

	/**
	 * @param x The anisotropy. x[0] is the angle, x[1] is the intensity of the anisotropy.
	 * @return The estimation of the spatial field at the locations, assumin an anisotropy x.
	 */
        const VectorXr fHat(const TVector & x) const;

    public:
	/**
	 * @param mesh TODO
	 * @param regressionData
	 * @param locations
	 */
        H(const MeshHandler<1> & mesh, const RegressionDataElliptic & regressionData, const std::vector<Point> & locations) : cppoptlib::BoundedProblem<Real, 2>(), mesh_(mesh), regressionData_(regressionData), locations_(locations) {}

	/**
	 * @param x The anisotropy. x[0] is the angle, x[1] is the intensity of the anisotropy.
	 * @return The anisotropy in matrix form.
	 */
        static Eigen::Matrix<Real, 2, 2> buildKappa(const TVector & x);

	/**
	 * @param x The anisotropy. x[0] is the angle, x[1] is the intensity of the anisotropy.
	 * @return The value of the functional H(K). \cite Bernardi.
	 */
        Real value(const TVector & x);
};

#endif
