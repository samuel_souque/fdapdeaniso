#include "optimH.h"

#include <cmath>

void OptimH::minimize(H & h, TVector & start) {
    // Define (α_min, ɣ_min)
    const TVector lowerBound(0., 1.);
    // Define (α_max, ɣ_max)
    const TVector upperBound(M_PI, 1000.);
    // Set the constraints
    h.setBoxConstraint(lowerBound, upperBound);

    // L-BFGS-B solver from CppOptLib
    cppoptlib::LbfgsbSolver<H> solver;
    // Minimize and update the value of start
    solver.minimize(h, start);
    if (start(1) < 0.) {
        start(1) = -start(1);
    }
    start = start.cwiseMax(lowerBound).cwiseMin(upperBound);
}
