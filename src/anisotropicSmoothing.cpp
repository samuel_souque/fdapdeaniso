#include "anisotropicSmoothing.h"

#include <cmath>
#include <algorithm>
#include "optimH.h"
#include "j.h"

std::pair<const std::vector<VectorXr>, const Eigen::Matrix<Real, 2, 2>> AnisotropicSmoothing::smooth() const {
    std::vector<Real>::size_type n_lambda = lambda_.size();
    std::vector<H::TVector, Eigen::aligned_allocator<H::TVector>> kSmooth(n_lambda, H::TVector(M_PI_2, 5));
    std::vector<Eigen::Index> rhoSmoothInd(n_lambda);
    std::vector<Real> gcvSmooth(n_lambda);

    #pragma omp parallel for
    for(std::vector<Real>::size_type i = 0U; i < n_lambda; i++) {
        // Optimization of H
        RegressionDataElliptic dataUniqueLambda = createRegressionData(lambda_[i]);
        H h(mesh_, dataUniqueLambda, locations_);

        OptimH::minimize(h, kSmooth[i]);

        // Dealing with angle periodicity
        if (kSmooth[i](0) == M_PI) {
            kSmooth[i](0) = 0.;
            OptimH::minimize(h, kSmooth[i]);
            REprintf("Angle equal to pi at iteration = %3u\n", i);
        }
        if (kSmooth[i](0) == 0.) {
            kSmooth[i](0) = M_PI;
            OptimH::minimize(h, kSmooth[i]);
            REprintf("Angle equal to 0 at iteration = %3u\n", i);
        }

        // Computation of the GCV for current k
        RegressionDataElliptic dataSelectedK = createRegressionData(kSmooth[i], true);
        J j(mesh_, dataSelectedK, locations_);

        VectorXr gcvSeq = j.getGCV();
        Eigen::Index rhoIndex;
        Real gcv = gcvSeq.minCoeff(&rhoIndex);

        //kSmooth[i] = k;
        rhoSmoothInd[i] = rhoIndex;
        gcvSmooth[i] = gcv;

    }

    // Choosing the best anisotropy matrix and lambda coefficient
    std::vector<Real>::const_iterator minIterator = std::min_element(gcvSmooth.cbegin(), gcvSmooth.cend());
    std::vector<Real>::difference_type optIndex = std::distance(gcvSmooth.cbegin(), minIterator);

    Rprintf("Optimal index = %3d\n", optIndex);
    Rprintf("Final k = %8f %8f, Final rho = %8f\n", kSmooth[optIndex](0), kSmooth[optIndex](1), regressionData_.getLambda()[rhoSmoothInd[optIndex]]);

    RegressionDataElliptic regressionDataFinal = createRegressionData(regressionData_.getLambda()[rhoSmoothInd[optIndex]], kSmooth[optIndex]);
    MixedFERegression<RegressionDataElliptic, IntegratorTriangleP2, 1> regressionFinal(mesh_, regressionDataFinal);
    
    regressionFinal.apply();

    return std::make_pair(regressionFinal.getSolution(), regressionDataFinal.getK());
}

RegressionDataElliptic AnisotropicSmoothing::createRegressionData(const Real & lambda) const {
    std::vector<Point> empty = regressionData_.getLocations();
    VectorXr observations = regressionData_.getObservations();
    std::vector<Real> uniqueLambda(1U, lambda);
    Eigen::Matrix<Real, 2, 2> kappa;
    Eigen::Matrix<Real, 2, 1> beta = regressionData_.getBeta();
    MatrixXr covariates = regressionData_.getCovariates();
    std::vector<UInt> dirichletIndices = regressionData_.getDirichletIndices();
    std::vector<Real> dirichletValues = regressionData_.getDirichletValues();

    const RegressionDataElliptic result(
            empty,
            observations,
            regressionData_.getOrder(),
            uniqueLambda,
            kappa,
            beta,
            regressionData_.getC(),
            covariates,
            dirichletIndices,
            dirichletValues,
            regressionData_.computeDOF());

    return result;
}

RegressionDataElliptic AnisotropicSmoothing::createRegressionData(const H::TVector & k, const bool dof) const {
    std::vector<Point> empty = regressionData_.getLocations();
    VectorXr observations = regressionData_.getObservations();
    Eigen::Matrix<Real, 2, 2> kappa = H::buildKappa(k);
    Eigen::Matrix<Real, 2, 1> beta = regressionData_.getBeta();
    MatrixXr covariates = regressionData_.getCovariates();
    std::vector<UInt> dirichletIndices = regressionData_.getDirichletIndices();
    std::vector<Real> dirichletValues = regressionData_.getDirichletValues();

    const RegressionDataElliptic result(
            empty,
            observations,
            regressionData_.getOrder(),
            regressionData_.getLambda(),
            kappa,
            beta,
            regressionData_.getC(),
            covariates,
            dirichletIndices,
            dirichletValues,
            dof);

    return result;
}

RegressionDataElliptic AnisotropicSmoothing::createRegressionData(const Real & lambda, const H::TVector & k) const {
    std::vector<Point> empty = regressionData_.getLocations();
    VectorXr observations = regressionData_.getObservations();
    std::vector<Real> uniqueLambda(1U, lambda);
    Eigen::Matrix<Real, 2, 2> kappa = H::buildKappa(k);
    Eigen::Matrix<Real, 2, 1> beta = regressionData_.getBeta();
    MatrixXr covariates = regressionData_.getCovariates();
    std::vector<UInt> dirichletIndices = regressionData_.getDirichletIndices();
    std::vector<Real> dirichletValues = regressionData_.getDirichletValues();

    const RegressionDataElliptic result(
            empty,
            observations,
            regressionData_.getOrder(),
            uniqueLambda,
            kappa,
            beta,
            regressionData_.getC(),
            covariates,
            dirichletIndices,
            dirichletValues,
            regressionData_.computeDOF());

    return result;
}
