#ifndef __EVALUATOR_EXT_HPP__
#define __EVALUATOR_EXT_HPP__

#include "fdaPDE.h"
#include "mesh.h"
#include "evaluator.h"


/**
 * @class EvaluatorExt
 * @brief allows to conveniently transform the solution vector of coefficients to the estimation fHat.
 */
class EvaluatorExt : public Evaluator<1> {
    public:
        EvaluatorExt(const MeshHandler<1> & mesh) : Evaluator(mesh), numNodes_(mesh.num_nodes()) {}

        /**
		 * @param solution the vector of solution coefficients as returned by the method MixedFERegression::apply()
		 * @return The vector of estimations fHat.
		 */
		// TODO Why not return a VectorXr ?
        const MatrixXr eval(const std::vector<VectorXr> & solution, const std::vector<Point> & locations);

    private:
        const UInt numNodes_;
};

#endif
