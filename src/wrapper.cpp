#include "fdaPDE.h"
#include "mesh.h"
#include "regressionData.h"
#include "anisotropicSmoothing.h"

/**
 * @file wrapper.cpp
 * @brief contains the c++ side wrapper of the package
 */

#ifdef R_VERSION_
void setLambda(SEXP Rlambda, std::vector<Real> & lambda) {
    UInt length = Rf_length(Rlambda);
    lambda.reserve(length);
    for (UInt i=0; i<length; i++) {
        lambda.push_back(REAL(Rlambda)[i]);
    }
}

void setLocations(SEXP Rlocations, std::vector<Point> & locations) {
    UInt length = INTEGER(Rf_getAttrib(Rlocations, R_DimSymbol))[0];
    locations.reserve(length);
    for (UInt i=0; i<length; i++) {
        Real * loc = REAL(Rlocations);
        locations.emplace_back(loc[i/* + length*0*/], loc[i + length*1]);
    }
}

extern "C" {
    //regression_PDE(SEXP Rlocations, SEXP Robservations, SEXP Rmesh, SEXP Rorder, SEXP Rlambda, SEXP RK, SEXP Rbeta, SEXP Rc, SEXP Rcovariates, SEXP RBCIndices, SEXP RBCValues, SEXP DOF)
    SEXP wrapper(SEXP Rlocations, SEXP Robservations, SEXP Rmesh, SEXP Rorder, SEXP Rlambda, SEXP Rrho, SEXP RK, SEXP Rbeta, SEXP Rc, SEXP Rcovariates, SEXP RBCIndices, SEXP RBCValues, SEXP DOF) {
        MeshHandler<1> mesh(Rmesh);
        RegressionDataElliptic regressionData(Rf_allocMatrix(REALSXP, 0, 2), Robservations, Rorder, Rrho, RK, Rbeta, Rc, Rcovariates, RBCIndices, RBCValues, DOF);

        std::vector<Real> lambda;
        setLambda(Rlambda, lambda);

        std::vector<Point> locations;
        setLocations(Rlocations, locations);


		/* Execute the main algorithm and store the results:
			1) The vector of coefficients
			2) The anisotropy matrix K
		*/
        const AnisotropicSmoothing anisoSmooth(mesh, regressionData, lambda, locations);
        const std::pair<const std::vector<VectorXr>, const Eigen::Matrix<Real, 2, 2>> & solution = anisoSmooth.smooth();
        const std::vector<VectorXr> & coefs = std::get<0>(solution);
        const Eigen::Matrix<Real, 2, 2> & kappa = std::get<1>(solution);


		/* Put the results into an R list for safe return
		*/
        SEXP result = NILSXP;
        result = PROTECT(Rf_allocVector(VECSXP, 2));
        SET_VECTOR_ELT(result, 0, Rf_allocMatrix(REALSXP, coefs[0].size(), coefs.size()));
        SET_VECTOR_ELT(result, 1, Rf_allocMatrix(REALSXP, 2, 2));
        Real * rans = REAL(VECTOR_ELT(result, 0));
        for (Eigen::Index i=0; i<coefs[0].size(); i++) {
            for (std::vector<VectorXr>::size_type j=0; j<coefs.size(); j++) {
                rans[i + j*coefs[0].size()] = coefs[j](i);
            }
        }

        Real * rans2 = REAL(VECTOR_ELT(result, 1));
        for (UInt i=0; i<2; i++) {
            for (UInt j=0; j<2; j++) {
                rans2[i + 2*j] = kappa(i,j);
            }
        }

        UNPROTECT(1);
        return result;
    }
}
#endif
