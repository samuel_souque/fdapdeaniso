#include "j.h"

#include "evaluatorExt.h"
#include "mesh_objects.h"

J::J(const MeshHandler<1> & mesh, const RegressionDataElliptic & regressionData, const std::vector<Point> & locations) : mesh_(mesh), regressionData_(regressionData), regression_(mesh_, regressionData_), locations_(locations) {
    regression_.apply();
}

const std::vector<VectorXr> & J::getSolution() const {
    return regression_.getSolution();
}

const std::vector<Real> & J::getDOF() const {
    return regression_.getDOF();
}

VectorXr J::getGCV() const {
    const std::vector<VectorXr> & solution = getSolution();

    EvaluatorExt evaluator(mesh_);
    const MatrixXr & fnhat = evaluator.eval(solution, locations_);

    Real np = locations_.size();
    const std::vector<Real> & edf = getDOF();

    for (std::size_t i=0; i<edf.size(); i++) {
        if (np - edf[i] <= 0) {
            std::cerr << "Some values of 'edf' are inconstistent. This might be due to ill-conditioning of the linear system. Try increasing value of 'lambda'." << std::endl;
            break;
        }
    }
    
    using ArrayXXr = Eigen::Array<Real, Eigen::Dynamic, Eigen::Dynamic>;
    using ArrayXr = Eigen::Array<Real, Eigen::Dynamic, 1>;

    const VectorXr & observations = regressionData_.getObservations();

    const MatrixXr diff = fnhat.colwise() - observations;
    const ArrayXXr quotient = np / (np - Eigen::Map<const ArrayXr>(edf.data(), edf.size())).pow(2);

    VectorXr gcv = (quotient * (diff.transpose() * diff).diagonal().array()).matrix();

    return gcv;
}
