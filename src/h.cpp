#include "h.h"

#include "evaluatorExt.h"
#include "mixedFERegression.h"
#include "mesh_objects.h"

Real H::value(const TVector & x) {
    //if (!x.allFinite()) {
    //    REprintf("Non finite value of angle or intensity for H(k)\n");
    //}
    TVector k = x;
    if (k(1) < 0.) {
        k(1) = -k(1);
        REprintf("Inversion of intensity argument in H (angle = %8f, intensity = %8f)\n", x(0), x(1));
    }

    /*Calculating fHat*/
    VectorXr estimations = fHat(k);

    //if (!estimations.allFinite()) {
    //    REprintf("Non finite value in estimation fHat(k)\n");
    //}

    /* Calculating H */
    estimations -= regressionData_.getObservations();
    Real H = estimations.squaredNorm();
    //if (!std::isfinite(H)) {
    //    REprintf("H is not finite\n");
    //    H = std::numeric_limits<double>::max();
    //}
    return H;
}

const VectorXr H::fHat(const TVector & x) const {
    // Copy members of regressionData to pass them to its constructor
    std::vector<Point> empty = regressionData_.getLocations();
    VectorXr observations = regressionData_.getObservations();
    Eigen::Matrix<Real, 2, 2> kappa = buildKappa(x);
    Eigen::Matrix<Real, 2, 1> beta = regressionData_.getBeta();
    MatrixXr covariates = regressionData_.getCovariates();
    std::vector<UInt> dirichletIndices = regressionData_.getDirichletIndices();
    std::vector<Real> dirichletValues = regressionData_.getDirichletValues();

    // Construct a new regressionData object with the desired kappa
    const RegressionDataElliptic data(
            empty,
            observations,
            regressionData_.getOrder(),
            regressionData_.getLambda(),
            kappa,
            beta,
            regressionData_.getC(),
            covariates,
            dirichletIndices,
            dirichletValues,
            regressionData_.computeDOF());

    // Compute the regression coefficients
    MixedFERegression<RegressionDataElliptic, IntegratorTriangleP2, 1> regression(mesh_, data);
    regression.apply();
    const std::vector<VectorXr> & solution = regression.getSolution();
    
    // Return the evaluation of the coefficients at the data points
    EvaluatorExt evaluator(mesh_);
    const VectorXr result = evaluator.eval(solution, locations_);

    return result;
}

Eigen::Matrix<Real, 2, 2> H::buildKappa(const TVector & x) {
    // x[0] is the angle α (alpha)
    // x[1] is the intensity γ (gamma)

    //Building K from x as an Eigen matrix
    Eigen::Matrix<Real,2,2> Q;
    Q << std::cos(x[0]), -std::sin(x[0]),
      std::sin(x[0]), std::cos(x[0]);

    Eigen::Matrix<Real,2,2> Sigma;
    Sigma << 1/std::sqrt(x[1]), 0,
          0, x[1]/std::sqrt(x[1]);

    const Eigen::Matrix<Real, 2, 2> Kappa = Q * Sigma * Q.inverse();
    return Kappa;
}
